# -*- coding: utf-8 -*-
import sys
import os
from pyvis.network import Network
from bs4 import BeautifulSoup
import textract
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtCore import QUrl
import fitz
import string  

import pandas as pd
import networkx as nx

import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
stopWordsFr = stopwords.words('french')
from nltk import bigrams
import collections

import spacy
nlp = spacy.load("fr_core_news_sm")

from UI_request import Ui_RequestWindow

mainDir = os.path.dirname(os.path.realpath(__file__))
        
    
        
class RequestWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(RequestWindow, self).__init__(*args, **kwargs)
        self.ui = Ui_RequestWindow()
        self.ui.setupUi(self)
        self.webview = QWebEngineView()
        self.ui.GraphLayout.addWidget(self.webview)
        self.graphManager = GraphManager(self.webview)
        
        self.ui.SelectDocButton.triggered.connect(self.SelectDocs)
        self.ui.actionImporter_Graph.triggered.connect(self.ImportGraph)
        self.ui.actionExporter_Graph.triggered.connect(self.ExportGraph)
        
        self.listFilesPath = []
        self.bigram_df = None
        
    def Tokenize(self,sentence):
        # Tokeniser la phrase
        doc = nlp(sentence)
        # Retourner le texte de chaque token
        return [(X.text,X.pos_) for X in doc]
    
    def SelectDocs(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        self.listFilesPath, _ = QFileDialog.getOpenFileNames(self,"QFileDialog.getOpenFileNames()", "","Document Files (*.txt *.pdf *.doc *.docx)", options=options)
        if self.listFilesPath:
            totalBigrams = []
            for i in range(len(self.listFilesPath)):
                extension = os.path.splitext(self.listFilesPath[i])[1].lower()
                #fileName = ntpath.basename(self.listFilesPath[i])
                fileContent = ""
                
                if extension == ".txt":
                    file = open(self.listFilesPath[i],'r')
                    fileContent = file.read()
                    file.close()
                elif extension == ".doc" or extension == ".docx":
                    fileContent = textract.process(self.listFilesPath[i]).decode()
                elif extension == ".pdf":
                    
                    fileContent = ""
                    
                    with fitz.open(self.listFilesPath[i]) as doc:
                        for page in doc:
                            fileContent += page.getText()
                            
                words = fileContent.lower().replace("\n", " ")
                doc = self.Tokenize(words)
                words_without_sw = []
                
                #[word for word in doc if not word in stopwords.words('french')]
                
                for token in doc:
                    if token[0] not in stopWordsFr and token[1] == 'NOUN' and len(token[0])>2:
                        words_without_sw.append(token[0])
                
                totalBigrams.extend(list(bigrams(words_without_sw)))
            
            bigram_counts = collections.Counter(totalBigrams)
            
            bigram_result = []
            for bigram in bigram_counts.items():
                bigram_result.append((bigram[0][0],bigram[0][1],bigram[1]))
            
            
            self.bigram_df = pd.DataFrame(bigram_result,
                             columns=['source', 'target', 'label'])
            
            self.ShowGraph()
        
    def ShowGraph(self):
        self.graphManager.GenerateGraph(self.bigram_df)
    
    def ExportGraph(self):
        if self.bigram_df is not None:
            if not self.bigram_df.empty:
                options = QFileDialog.Options()
                #options |= QFileDialog.DontUseNativeDialog
                fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","Dataframe (*.df)", options=options)
                if fileName:
                    print(fileName)
                    self.bigram_df.to_csv(fileName,index=False)
        
    def ImportGraph(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","Dataframe (*.df)", options=options)
        if fileName:
            self.bigram_df = pd.read_csv(fileName,index_col=False)
            self.ShowGraph()


        

class GraphManager:
    def __init__(self, webview):
        self.webview = webview
        self.graph = Network()
        self.graph.set_options('var options = {"autoResize": true, "height": "100%", "width": "100%", "locale": "fr", "clickToUse": false}')
        
        self.__custom_style__ = """
            body{
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #222222;}
            h1{visibility: collapse;}
            #mynetwork {
                width: 100%;
                height: 100%;
                background-color: #222222;
                border: 0;
                position: absolute;
                top: 0;
                left: 0;}
        """
        
    def GenerateGraph(self, data):
        G = nx.from_pandas_edgelist(data, edge_attr=True)
        
        G_dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
        G_dyn.from_nx(G)
        
        #G_dyn.show_buttons(filter_=['physics'])
        
        voisin = G_dyn.get_adj_list()
        for node in G_dyn.nodes :
            node["size"] = 5 * len(voisin[node["id"]])
        
        self.graph = G_dyn
        #self.graph.barnes_hut(gravity=-40000, central_gravity=0.3, spring_length=250, spring_strength=0.01, damping=0.09, overlap=0)
        self.graph.save_graph("graph.html")
        graph_path = os.path.abspath(os.path.join(mainDir, "graph.html"))
        
        soup = BeautifulSoup(open("graph.html").read(),features="html.parser")
        style_tag = soup.find("style")
        style_tag.string = self.__custom_style__
        open("graph.html", "w", encoding="utf-8").write(str(soup))
        
        self.webview.load(QUrl.fromLocalFile(graph_path))
        

if __name__ == '__main__':
    
    #SETUP APP
    app = QApplication(sys.argv)
    themeFile = "darkorange.qss"
    with open(themeFile,"r") as theme:
        app.setStyleSheet(theme.read())
    
    #SETUP WINDOWS
    request_win = RequestWindow()

    #DISPLAY WINDOWS
    request_win.show()
    
    app.exec_()
    sys.exit()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    