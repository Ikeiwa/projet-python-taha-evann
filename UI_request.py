# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\UI\RequestWindow.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RequestWindow(object):
    def setupUi(self, RequestWindow):
        RequestWindow.setObjectName("RequestWindow")
        RequestWindow.setEnabled(True)
        RequestWindow.resize(761, 798)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\UI\\icon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        RequestWindow.setWindowIcon(icon)
        self.MainGrid = QtWidgets.QWidget(RequestWindow)
        self.MainGrid.setObjectName("MainGrid")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.MainGrid)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.GraphLayout = QtWidgets.QGridLayout()
        self.GraphLayout.setObjectName("GraphLayout")
        self.verticalLayout.addLayout(self.GraphLayout)
        self.verticalLayout_3.addLayout(self.verticalLayout)
        RequestWindow.setCentralWidget(self.MainGrid)
        self.menuBar = QtWidgets.QMenuBar(RequestWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 761, 21))
        self.menuBar.setObjectName("menuBar")
        self.menuSelectioner_les_documents = QtWidgets.QMenu(self.menuBar)
        self.menuSelectioner_les_documents.setObjectName("menuSelectioner_les_documents")
        RequestWindow.setMenuBar(self.menuBar)
        self.actionEnregistrer = QtWidgets.QAction(RequestWindow)
        self.actionEnregistrer.setObjectName("actionEnregistrer")
        self.actionQuitter = QtWidgets.QAction(RequestWindow)
        self.actionQuitter.setObjectName("actionQuitter")
        self.SelectDocButton = QtWidgets.QAction(RequestWindow)
        self.SelectDocButton.setObjectName("SelectDocButton")
        self.actionImporter_Graph = QtWidgets.QAction(RequestWindow)
        self.actionImporter_Graph.setObjectName("actionImporter_Graph")
        self.actionExporter_Graph = QtWidgets.QAction(RequestWindow)
        self.actionExporter_Graph.setObjectName("actionExporter_Graph")
        self.menuSelectioner_les_documents.addAction(self.SelectDocButton)
        self.menuSelectioner_les_documents.addAction(self.actionImporter_Graph)
        self.menuSelectioner_les_documents.addAction(self.actionExporter_Graph)
        self.menuBar.addAction(self.menuSelectioner_les_documents.menuAction())

        self.retranslateUi(RequestWindow)
        QtCore.QMetaObject.connectSlotsByName(RequestWindow)

    def retranslateUi(self, RequestWindow):
        _translate = QtCore.QCoreApplication.translate
        RequestWindow.setWindowTitle(_translate("RequestWindow", "DataViz"))
        self.menuSelectioner_les_documents.setTitle(_translate("RequestWindow", "Fichier"))
        self.actionEnregistrer.setText(_translate("RequestWindow", "Enregistrer"))
        self.actionQuitter.setText(_translate("RequestWindow", "Quitter"))
        self.SelectDocButton.setText(_translate("RequestWindow", "Ouvrir documents"))
        self.actionImporter_Graph.setText(_translate("RequestWindow", "Importer Graph"))
        self.actionExporter_Graph.setText(_translate("RequestWindow", "Exporter Graph"))
