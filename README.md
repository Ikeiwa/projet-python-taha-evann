# Projet Python Taha Evann  
  
  
  
# Pour lancer l'application :  
> python Main.py  

ou sur windows ouvrir "START APP.bat"  
  
# Pour utiliser l'application : 
  
Pour générer un graph à partir d'un corpus :  
Fichier -> Ouvrir documents  
  
Vous pouvez importer ou exporter un graph avec  
Fichier->Importer et Fichier->Exporter  
  
# librairie nécéssaires:  
  
-PyQt5  
-pyvis  
-beautifulsoup4  
-panda  
-nltk  
-textract  
-spacy  
-PyMuPDF  